package ru.t1.ktitov.tm.api.controller;

public interface ITaskController {

    void createTask();

    void clearTasks();

    void showTasks();

    void showTaskById();

    void showTaskByIndex();

    void removeTaskById();

    void removeTaskByIndex();

    void updateTaskById();

    void updateTaskByIndex();

    void startTaskById();

    void startTaskByIndex();

    void completeTaskById();

    void completeTaskByIndex();

    void changeTaskStatusById();

    void changeTaskStatusByIndex();

}
