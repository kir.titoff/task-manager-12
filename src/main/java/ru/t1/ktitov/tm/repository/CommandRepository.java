package ru.t1.ktitov.tm.repository;

import ru.t1.ktitov.tm.api.repository.ICommandRepository;
import ru.t1.ktitov.tm.constant.ArgumentConst;
import ru.t1.ktitov.tm.constant.TerminalConst;
import ru.t1.ktitov.tm.model.Command;

public final class CommandRepository implements ICommandRepository {

    public final static Command PROJECT_CLEAR = new Command(
            TerminalConst.PROJECT_CLEAR, null,
            "Remove all projects"
    );

    public final static Command PROJECT_LIST = new Command(
            TerminalConst.PROJECT_LIST, null,
            "Show all projects"
    );

    public final static Command PROJECT_CREATE = new Command(
            TerminalConst.PROJECT_CREATE, null,
            "Create new project"
    );

    public final static Command PROJECT_SHOW_BY_ID = new Command(
            TerminalConst.PROJECT_SHOW_BY_ID, null,
            "Show project by id"
    );

    public final static Command PROJECT_SHOW_BY_INDEX = new Command(
            TerminalConst.PROJECT_SHOW_BY_INDEX, null,
            "Show project by index"
    );

    public final static Command PROJECT_UPDATE_BY_ID = new Command(
            TerminalConst.PROJECT_UPDATE_BY_ID, null,
            "Update project by id"
    );

    public final static Command PROJECT_UPDATE_BY_INDEX = new Command(
            TerminalConst.PROJECT_UPDATE_BY_INDEX, null,
            "Update project by index"
    );

    public final static Command PROJECT_REMOVE_BY_ID = new Command(
            TerminalConst.PROJECT_REMOVE_BY_ID, null,
            "Remove project by id"
    );

    public final static Command PROJECT_REMOVE_BY_INDEX = new Command(
            TerminalConst.PROJECT_REMOVE_BY_INDEX, null,
            "Remove project by index"
    );

    public final static Command PROJECT_START_BY_ID = new Command(
            TerminalConst.PROJECT_START_BY_ID, null,
            "Start project by id"
    );

    public final static Command PROJECT_START_BY_INDEX = new Command(
            TerminalConst.PROJECT_START_BY_INDEX, null,
            "Start project by index"
    );

    public final static Command PROJECT_COMPLETE_BY_ID = new Command(
            TerminalConst.PROJECT_COMPLETE_BY_ID, null,
            "Complete project by id"
    );

    public final static Command PROJECT_COMPLETE_BY_INDEX = new Command(
            TerminalConst.PROJECT_COMPLETE_BY_INDEX, null,
            "Complete project by index"
    );

    public final static Command PROJECT_CHANGE_STATUS_BY_ID = new Command(
            TerminalConst.PROJECT_CHANGE_STATUS_BY_ID, null,
            "Change project status by id"
    );

    public final static Command PROJECT_CHANGE_STATUS_BY_INDEX = new Command(
            TerminalConst.PROJECT_CHANGE_STATUS_BY_INDEX, null,
            "Change project status by index"
    );

    public final static Command TASK_CLEAR = new Command(
            TerminalConst.TASK_CLEAR, null,
            "Remove all tasks"
    );

    public final static Command TASK_LIST = new Command(
            TerminalConst.TASK_LIST, null,
            "Show all tasks"
    );

    public final static Command TASK_CREATE = new Command(
            TerminalConst.TASK_CREATE, null,
            "Create new task"
    );

    public final static Command TASK_SHOW_BY_ID = new Command(
            TerminalConst.TASK_SHOW_BY_ID, null,
            "Show task by id"
    );

    public final static Command TASK_SHOW_BY_INDEX = new Command(
            TerminalConst.TASK_SHOW_BY_INDEX, null,
            "Show task by index"
    );

    public final static Command TASK_UPDATE_BY_ID = new Command(
            TerminalConst.TASK_UPDATE_BY_ID, null,
            "Update task by id"
    );

    public final static Command TASK_UPDATE_BY_INDEX = new Command(
            TerminalConst.TASK_UPDATE_BY_INDEX, null,
            "Update task by index"
    );

    public final static Command TASK_REMOVE_BY_ID = new Command(
            TerminalConst.TASK_REMOVE_BY_ID, null,
            "Remove task by id"
    );

    public final static Command TASK_REMOVE_BY_INDEX = new Command(
            TerminalConst.TASK_REMOVE_BY_INDEX, null,
            "Remove task by index"
    );

    public final static Command TASK_START_BY_ID = new Command(
            TerminalConst.TASK_START_BY_ID, null,
            "Start task by id"
    );

    public final static Command TASK_START_BY_INDEX = new Command(
            TerminalConst.TASK_START_BY_INDEX, null,
            "Start task by index"
    );

    public final static Command TASK_COMPLETE_BY_ID = new Command(
            TerminalConst.TASK_COMPLETE_BY_ID, null,
            "Complete task by id"
    );

    public final static Command TASK_COMPLETE_BY_INDEX = new Command(
            TerminalConst.TASK_COMPLETE_BY_INDEX, null,
            "Complete task by index"
    );

    public final static Command TASK_CHANGE_STATUS_BY_ID = new Command(
            TerminalConst.TASK_CHANGE_STATUS_BY_ID, null,
            "Change task status by id"
    );

    public final static Command TASK_CHANGE_STATUS_BY_INDEX = new Command(
            TerminalConst.TASK_CHANGE_STATUS_BY_INDEX, null,
            "Change task status by index"
    );

    private final static Command VERSION = new Command(
            TerminalConst.VERSION, ArgumentConst.VERSION,
            "Show application version"
    );

    private final static Command ABOUT = new Command(
            TerminalConst.ABOUT, ArgumentConst.ABOUT,
            "Show developer info"
    );

    private final static Command HELP = new Command(
            TerminalConst.HELP, ArgumentConst.HELP,
            "Show application commands"
    );

    private final static Command EXIT = new Command(
            TerminalConst.EXIT, null,
            "Exit"
    );

    private final static Command INFO = new Command(
            TerminalConst.INFO, ArgumentConst.INFO,
            "Show available memory"
    );

    private final static Command COMMANDS = new Command(
            TerminalConst.COMMANDS, ArgumentConst.COMMANDS,
            "Show available commands"
    );

    private final static Command ARGUMENTS = new Command(
            TerminalConst.ARGUMENTS, ArgumentConst.ARGUMENTS,
            "Show available arguments"
    );

    private final static Command[] TERMINAL_COMMANDS = new Command[]{
            VERSION, ABOUT, HELP, INFO,
            COMMANDS, ARGUMENTS, EXIT,
            PROJECT_CLEAR, PROJECT_LIST, PROJECT_CREATE,
            PROJECT_SHOW_BY_ID, PROJECT_SHOW_BY_INDEX, PROJECT_REMOVE_BY_ID,
            PROJECT_REMOVE_BY_INDEX, PROJECT_UPDATE_BY_ID, PROJECT_UPDATE_BY_INDEX,
            PROJECT_START_BY_ID, PROJECT_START_BY_INDEX, PROJECT_COMPLETE_BY_ID,
            PROJECT_COMPLETE_BY_INDEX, PROJECT_CHANGE_STATUS_BY_ID, PROJECT_CHANGE_STATUS_BY_INDEX,
            TASK_CLEAR, TASK_LIST, TASK_CREATE,
            TASK_SHOW_BY_ID, TASK_SHOW_BY_INDEX, TASK_REMOVE_BY_ID,
            TASK_REMOVE_BY_INDEX, TASK_UPDATE_BY_ID, TASK_UPDATE_BY_INDEX,
            TASK_START_BY_ID, TASK_START_BY_INDEX, TASK_COMPLETE_BY_ID,
            TASK_COMPLETE_BY_INDEX, TASK_CHANGE_STATUS_BY_ID, TASK_CHANGE_STATUS_BY_INDEX
    };

    @Override
    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}
